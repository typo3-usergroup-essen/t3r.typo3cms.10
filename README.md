27.05.2020 – Version 1.3.0

 

# Vorwort

Diese Ausarbeitung dient als Bei- und Nachschlagewerk einer Präsentation auf dem TYPO3 Stammtisch Ruhr im Mai 2020.

Die Authoren sind:

- **Andy Hausmann** von sota-studio.de
- **Ralf Hüskes** von ideegestalten.net

Wir lernten uns 2014 auf einem TYPO3 Stammtisch in Essen kennen und arbeiten seither zusammen an Projekten.



Mit diesem Projekt möchten wir den schnellen Einstieg in folgende Themen ermöglichen und demonstrieren, wie unkompliziert es gehen kann:

* Eine lokale Entwicklungsumgebung einrichten
* TYPO3 CMS 10 installieren und rudimentär einrichten
* Ein Site Package als Basis einer Website erstellen und einbinden
* Webpack Encore als Build Tool für CSS, JS, Fonts, Bilder, etc. einrichten
* Tailwind CSS im Build Tool und Sitepackage nutzen
* Vorhandene Inhaltselemente anpassen und inviduelle erstellen
* Eine eigene Extension erstellen und Datensätze über eine API verfügbar machen
* React in der eigenen Extension einbinden, Daten abrufen und darstellen

Bitte lass dich durch die Zeitangaben an den Kapiteln nicht verunsichern. Sie dienen uns während der Session als Orientierung.



Am Ende halten wir für dich ein paar Befehle und Links bereit, die zumindest unsere Leben stark vereinfacht haben. Wenn du dir sofort das Endergebnis ansehen willst, wirf einen Blick in

* Kapitel 1: Kickstart Local Development
* Anhang B: Projekt klonen



Viel Spaß!

 

# 1. Kickstart Local Development

**Vorgetragen von Andy Hausmann – ca. 15 Minuten.**

Was braucht man für eine moderne Webentwicklung? Brew, git und DDEV! Der Rest kommt ganz von allein, denn DDEV ist eine bereits optimale Container-Landschaft, die auf Docker basiert.

 

## 1.1. Homebrew installieren

Homebrew ist der fehlende Paketmanager für Macs. Nach einer unkomplizierten Installation, können darüber die unterschiedlichsten Software-Pakete und Bibliotheken bezogen werden. Und das Beste: Sie werden direkt optimal eingebunden und versioniert.

In anderen Worten: 

> Homebrew installiert Zeug, das du brauchst, das Apple aber nicht mitliefert.

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```

Dokumentation unter https://brew.sh/index_de.

 

## 1.2. git installieren

```bash
brew install git
```

 

## 1.3. DDEV installieren

```bash
brew tap drud/ddev && brew install ddev
```

Dokumentation unter https://ddev.readthedocs.io/en/stable/.

 

## 1.4. NPM installieren

```bash
brew install node
```

 

## 1.5. Yarn installieren

```bash
brew install yarn
```

Dokumentation unter https://classic.yarnpkg.com/en/docs/install/#mac-stable.

 

# 2. Installation TYPO3 CMS 10

**Vorgetragen von Andy Hausmann – ca. 15 Minuten.**

Der beste Weg um **TYPO3 CMS** zu installieren, verwalten und später aktuell zu halten, ist **composer**. Die **Paketverwaltung für php** ist perfekt – ganz gleich ob du lokal oder auf deiner Produktivmaschine, beim Hoster der Wahl, arbeitest.

Wichtiges Detail: Es wird direkt in die Container-Landschaft installiert. So stellst du sicher, dass bspw. deine installierte php-Version irrelevant ist. DDEV kümmert sich um alles!

 

## 2.1. Installation

```bash
mkdir T3r.TYPO3CMS.10
cd T3r.TYPO3CMS.10
ddev config --project-type php --php-version 7.3
ddev composer create "typo3/cms-base-distribution:^10" --prefer-dist
ddev config --project-type=typo3
ddev restart
```

 

## 2.2. Einrichtung

https://tsrshowcase.ddev.site/ aufrufen und den Schritten folgen:

- Datei »**FIRST_INSTALL**« im **public-Verzeichnis** erstellen

  ```
  touch public/FIRST_INSTALL
  ```
  
- **Installationsanweisungen** befolgen

- **Startseite** im Seitenbaum anlegen

- **Root TypoScript Template** erstellen

- **Site** wie gewohnt konfigurieren



## 2.3. Erster Commit

Zunächst benötigst du eine ».gitignore«-Datei, damit auch keine unnötigen Dateien übertragen werden.

```bash
.DS_Store

.idea
nbproject

node_modules

/var/*
!/var/labels
/vendor
/public/*
!/public/.htaccess
!/public/typo3conf
/public/typo3conf/*
!/public/typo3conf/LocalConfiguration.php
!/public/typo3conf/AdditionalConfiguration.php
!/public/fileadmin/
/public/fileadmin/_processed_
/public/fileadmin/_temp_
/public/fileadmin/user_upload/_temp_

packages/site/Resources/Public/build/
```

Danach machen wir unseren ersten Commit:

```bash
git init
git remote add origin REPOSITORY_URL
mkdir -p build/Snapshots/ 
ddev export-db --gzip=false --file=build/Snapshots/$(date +%Y-%m-%d-%H%M).sql
git add .
git commit -m 'Initial commit'
git push -u origin master
```

 


# 3. Kickstart Site Package

**Vorgetragen von Ralf Hüskes – ca. 15 Minuten.**

Zu einer modernen Webentwicklung mit TYPO3 CMS gehört das Pattern eines »Site Package«. Dabei gilt die Faustregel: ein Site Package je Website, Microsite und Landingpage. Durch die Nutzung von Webpack Encore und einer sauberen Struktur, können sich Website problemlos Assets teilen.

Die ausführliche Anleitung findet Ihr in der [README_3.0_KickstartSitePackage.md](README_3.0_KickstartSitePackage.md)

 


# 4. Webpack Encore

**Vorgetragen von Andy Hausmann – ca. 15 Minuten.**

Webpack Encore ist ein Wrapper für Webpack, das komfortable Buildprozesse ermöglicht. Durch sogenannte »Entrypoints« lassen sich Bündel aus CSS, JS, Fonts und anderen Assets erstellen, die mit wenig Aufewand in TYPO3 CMS eingebunden werden können.

Webpack Encore ist aus der »Symfony«-Szene bekannt und kann in Form einer Extension (typo3_encore) vollständig in TYPO3 CMS integriert werden.

 

## 4.1. Webpack Encore installieren

Zunächst benötigen wir Webpack mit dem Encore Wrapper.

```bash
yarn add @symfony/webpack-encore --dev
yarn install
```

Dokumentation unter https://symfony.com/doc/current/frontend/encore/installation.html

 

## 4.2. Webpack Config

Damit Webpack überhaupt weiß, was es tun soll, benötigen wir eine Konfiguration. Diese Datei heißt **webpack.config.js** und gehört in das **Root-Verzeichnis**.

Hier ein Best Practice-Inhalt:

```javascript
var Encore = require('@symfony/webpack-encore');

if (!Encore.isRuntimeEnvironmentConfigured()) {
  Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
  .setOutputPath('packages/site/Resources/Public/build/')
  .setPublicPath('/typo3conf/ext/site/Resources/Public/build/')
  .setManifestKeyPrefix('build/')

  .addEntry('app', './assets/app.js')

  .splitEntryChunks()
  .enableSingleRuntimeChunk()
  .cleanupOutputBeforeBuild()
  .enableBuildNotifications()
  .enableSourceMaps(!Encore.isProduction())
  .enableVersioning(Encore.isProduction())
  .configureBabelPresetEnv((config) => {
    config.useBuiltIns = 'usage';
    config.corejs = 3;
  })
  .enableSassLoader()
  //.enableReactPreset()
;

module.exports = Encore.getWebpackConfig();
```

Dokumentation unter https://webpack.js.org/configuration/.

Ein paar Abhängigkeiten musst du noch installieren:

```go
yarn add sass-loader@^8.0.0 node-sass --dev
yarn add webpack-notifier@^1.6.0 --dev
```

 

## 4.3. TYPO3 Webpack Encore installieren

Und dann benötigen wir eine Extension für TYPO3 CMS, die Webpack Encore "versteht". Damit wir diese allerdings installieren können, müssen wir vorher die PHP-Version in der composer.json des Projektes auf 7.2.5 setzen. Danach:

```go
ddev composer req ssch/typo3-encore
```



## 4.4. Erster Entrypoint und Build

Jetzt haben wir schon fast alles für unseren ersten Build. Was uns noch fehlt: der Entrypoint aus der Webpack.config.js. Erstelle die Datei »**app.js**« und leg sie in den Ordner **assets** im Root-Verzeichnis. Fürs Erste bekommst die nur den folgenden Inhalt:

```js
console.log('Hallo Welt!');
```

Und schon kannst du deinen ersten Build erstellen:

```bash
yarn encore dev
```

Schau jetzt mal in den Ordner »**packages/site/Resources/Public/build**«. Hier solltest du jetzt diese Dateien finden:

* app.js
* entrypoints.json
* manifest.json
* runtime.js



## 4.5. Entrypoint in TYPO3 CMS einbinden

Jetzt muss Webpack Encore für TYPO3 CMS konfiguriert werden – es muss nämlich wissen, wo die Entrypoints. Und dafür sucht die Extension »typo3_encore« nach den Dateien »**entrypoints.json**« und »**manifest.json**«.

Füge hierfür folgendes in die TypoScript Konfiguration deines Site Package – fürs Erste einfach an das Ender der jeweiligen Dateien.

packages/site/Configuration/TypoScript/**constants.typoscript**:

```
plugin.tx_typo3encore {
  settings {
    entrypointJsonPath = EXT:site/Resources/Public/build/entrypoints.json
    manifestJsonPath = EXT:site/Resources/Public/build/manifest.json

    preload {
      enable = 1
      crossorigin =
    }
  }
}
```

packages/site/Configuration/TypoScript/**setup.typoscript**:

```
plugin.tx_typo3encore {
    settings {
        entrypointJsonPath = {$plugin.tx_typo3encore.settings.entrypointJsonPath}
        manifestJsonPath = {$plugin.tx_typo3encore.settings.manifestJsonPath}

        preload {
            enable = {$plugin.tx_typo3encore.settings.preload.enable}
            crossorigin = {$plugin.tx_typo3encore.settings.preload.crossorigin}
        }
    }
}

module.tx_typo3encore.settings < plugin.tx_typo3encore.settings
```

Und jetzt noch mal zur packages/site/Configuration/TypoScript/**setup.typoscript** – hier änderst die die Konfiguration wie folgt ab:

```
includeCSS {
		10 = typo3_encore:app
}

includeJSFooter {
		10 = typo3_encore:app
}
```

Die vorherigen Ressourcen benötigst du nicht mehr.

Leere jetzt deine Caches und ruf die Startseite auf. Schau in deine JavaScript-Konsole. Steht dort "Hallo Welt!", dann hast du es geschafft!



# 5. Tailwind CSS

**Vorgetragen von Andy Hausmann – ca. 15 Minuten.**

Tailwind CSS ist ein modernes, Komponenten-getriebenes CSS Framework, das eine Mange Spaß macht.
Wir verwenden in unserem Beispiel ein Template der Seite https://www.tailwindtoolbox.com/. 


## 5.1. Installation

```bash
yarn add tailwindcss postcss-loader postcss-import autoprefixer
```

Dokumentation unter https://tailwindcss.com/docs/installation/ 
und https://medium.com/better-programming/tailwindcss-and-symfonys-webpack-encore-7bfc8c18665b.



## 5.2. Konfiguration

* config/postcss.config.js erstellen:

  ```js
  let tailwindcss = require('tailwindcss');
  
  module.exports = {
      plugins: [
          tailwindcss('./config/tailwind.config.js'),
          require('autoprefixer'),
          require('postcss-import')
      ]
  };
  ```

* config/tailwind.config.js erstellen:

  ```js
  module.exports = {
      theme: {
          extend: {}
      },
      variants: {},
      plugins: [],
      prefix: [],
  };
  ```

* webpack.config.js erweitern um postcss loader:

  ```js
  […]
  		.enableSassLoader(function (options) {}, {
          resolveUrlLoader: false
      })
      .enablePostCssLoader(function(options) {
          options.config = {
              path: 'config/postcss.config.js'
          }
      })
  […]
  ```



## 5.3. Einbindung

Zuerst erstellst du eine neue Datei »assets/scss/tailwind.scss«:

```css
@import "~tailwindcss/base";
@import "~tailwindcss/components";
@import "~tailwindcss/utilities";
```

Und die bindest du dann in deiner app.js ein:

```js
require('./scss/tailwind.scss');
```

Und schon kannst du die Stile auch in deinem Site Package verwenden.



# 6. Tailwind im Site Package verwenden

**Vorgetragen von Ralf Hüskes – ca. 15 Minuten.**

In unserer Session zeigen wir dir in diesem Kapitel, wie du dein eigenes Design am Beispiel von Tailwind umsetzt. Wir nutzen dafür dieses Template: https://www.tailwindtoolbox.com/templates/landing-page.

Die ausführliche Anleitung findet Ihr in der [README_6.0_TailwindImSitePackage.md](README_6.0_TailwindImSitePackage.md)



# 7. Individuelle Inhaltselemente

**Vorgetragen von Ralf Hüskes – ca. 45 Minuten.**

In unserer Session zeigen wir dir:

* wie du ein bestehendes Core-Element um Funktionen ergänzt
* wie du ein eigenes, neues Inhaltselement erstellst

Die ausführliche Anleitung findet Ihr in der [README_7.0_IndividuelleInhaltselemente.md](README_7.0_IndividuelleInhaltselemente.md)

 

# 8. Eigene Extension mit JSON API

**Vorgetragen von Andy Hausmann – ca. 30 Minuten.**

In unserer Session führen wir dich durch ein kleines Extension Setup mit Einrichtung einer JSON API, um kontrolliert auf Daten deiner Extension von extern zugreifen zu können.

Für einen schnellen Start in deine Extension, nutzen wir den Extension Builder.



## 8.1. Extension Builder installieren

```bash
ddev composer require friendsoftypo3/extension-builder:dev-v10-compatibility
```



## 8.2. Kickstart einer neuen Extension

* Extension Builder aufrufen → Domain Modelling starten
* Extension Properties:
  * Name: Locations
  * Vendor Name: Typo3StammtischRuhr
  * Key: locations
* Frontend Plugin 1:
  * Name: Location List
  * Key: ll
* Frontend Plugin 2:
  * Name: Location Details
  * Key: ld
* Model Object:
  * Name: Location
  * Is Aggregate Root: true
  * Default Actions: list, show
  * Properties:
    * title: String
    * teaser: Text
    * description: Rich Text
    * street: String
    * streetNo: String
    * zip: String
    * city: String
    * country: String
    * phone: String
    * email: String
    * contactPerson: String
* Extension speichern



## 8.3. Extension einrichten

* Verschieben der Ext nach Ordner **packages**

* **composer.json** um version ergänzen und TYPO3 CMS Version anpassen auf ^10:

  ```js
  "version": "1.0.0",
  "require": {
  		"typo3/cms-core": "^10"
  },
  ```

* Via **composer** installieren:

  ```bash
  ddev composer req typo3stammtischruhr/locations
  ```

* **Datenbank**-Schema aktualisieren

* **Datenhaltung** für Datensätze erstellen: /Home/Datenhaltung/**Standorte**

* **TCA** packages/locations/Configuration/TCA/**tx_locations_domain_model_location.php** korrigieren und veraltete Eigenschaft t3ver_label entfernen

* Einen oder mehrere **Datensätze erstellen**

* **ext_localconf.php** öffnen und extensionName der Plugins mit ```Typo3StammtischRuhr.``` prefixen

* **List Plugin** auf neuer Seite **Standorte** anlegen und **Datenhaltung referenzierten**

* **TypoScript Constants** unter packages/site/Configuration/TypoScript/**constants.typoscript** einbinden:

  ```typoscript
  <INCLUDE_TYPOSCRIPT: source="FILE:EXT:locations/Configuration/TypoScript/constants.typoscript">
  ```

* **TypoScript SETUP** unter packages/site/Configuration/TypoScript/**constants.typoscript** einbinden:

  ```typoscript
  <INCLUDE_TYPOSCRIPT: source="FILE:EXT:locations/Configuration/TypoScript/setup.typoscript">
  ```

* **storagePid**-Erwähnungen in TypoScript Setup auskommentieren



## 8.4. Entrypoint »locations« für List View

* Entrypoint in webpack.config.js erstellen:

  ```js
  .addEntry('locations', './assets/locations.js')
  ```

* /assets/locations.js erstellen und mit Test-Output befüllen:

  ```js
  console.log('Locations');
  ```

* Kompilieren:

  ```bash
  yarn encore dev
  ```

* Entrypoint via **typo3_encore** in **List Plugin** einbinden:

  ```html
  {namespace e = Ssch\Typo3Encore\ViewHelpers}
  <html xmlns:f="http://typo3.org/ns/TYPO3/CMS/Fluid/ViewHelpers" 
        data-namespace-typo3-fluid="true">
  <f:layout name="Default" />
  
  <f:section name="content">
      <e:renderWebpackLinkTags entryName="locations"/>
      <e:renderWebpackScriptTags entryName="locations"/>
  
      <h1>Listing for Location</h1>
  
      <div id="root-locations"></div>
  </f:section>
  </html>
  ```



## 8.5. JSON API

* **API Controller** in packages/locations/Classes/Controller/**LocationApiController.php** erstellen:

  ```php
  <?php
  
  namespace Typo3StammtischRuhr\Locations\Controller;
  
  use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
  use TYPO3\CMS\Extbase\Mvc\View\JsonView;
  use Typo3StammtischRuhr\Locations\Domain\Repository\LocationRepository;
  
  class LocationApiController extends ActionController
  {
      /** @var JsonView */
      protected $view;
  
      /** @var string */
      protected $defaultViewObjectName = JsonView::class;
  
      /** @var LocationRepository */
      private $locationRepository;
  
      public function __construct(LocationRepository $locationRepository)
      {
          $this->locationRepository = $locationRepository;
      }
  
      public function listAction()
      {
          $locations = $this->locationRepository->findAll();
          $this->view->setConfiguration([
              'locations' => [
                  '_descendAll' => [
                      '_only' => ['uid', 'title', 'teaser'],
                  ],
              ],
          ]);
          $this->view->setVariablesToRender(['locations']);
          $this->view->assign('locations', $locations);
      }
  }
  ```

* **Autowire** für die Ext in packages/locations/Configuration/**Services.yaml** aktivieren:

  ```yaml
  services:
    _defaults:
      autowire: true
      autoconfigure: true
      public: false
  ```

* **API Plugin** in packages/locations/**ext_localconf.php** erstellen:

  ```php
  \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  	'Typo3StammtischRuhr.Locations',
  	'LlApi',
  	[
  		'LocationApi' => 'list, show',
  	],
  	[]
  );
  ```

* **API Route** in packages/locations/Configuration/TypoScript/**setup.typoscript** erstellen:

  ```typoscript
  # Plugin specific Configuration
  plugin.tx_locations_llapi.persistence.storagePid = 5
  plugin.tx_locations_llapi.persistence.recursive = 1
  
  # API Route
  locations_api = PAGE
  locations_api {
  	config {
      disableAllHeaderCode = 1
      debug = 0
      no_cache = 1
      additionalHeaders {
        10 {
          header = Content-Type: application/json
          replace = 1
        }
      }
    }
    typeNum = 50740101
    10 < tt_content.list.20.locations_llapi
  }
  ```

* Allgemeine Caches leeren und Ausgabe unter https://t3r.typo3cms.10.ddev.site/?type=50740101 prüfen

* **API Route** in Template packages/locations/Resources/Private/Templates/Location/**List.html** einbinden:

  ```html
  <script>
  	var ROUTE_LOCATIONS = "{f:uri.page(pageUid: 1, absolute: 1, additionalParams: {type: '50740101'})}";
  </script>
  
  ```



## 8.6. Weiterführende Links

* JSON View konfigurieren: https://docs.typo3.org/m/typo3/book-extbasefluid/master/en-us/8-Fluid/2-using-different-output-formats.html



# 9. React App mit API-Anbindung

**Vorgetragen von Andy Hausmann – ca. 30 Minuten.**

In unserer Session zeigen wir dir, wie du eine React App erstellst und an eine Extension API koppelst.



## 9.1. React installieren

* **React Preset** in **webpack.config.js** aktivieren  → .enableReactPreset())

* **React** installieren:

  ```bash
  yarn add @babel/preset-react@^7.0.0 --dev
  yarn add react react-dom core-js
  ```



## 9.2. React App

Erstelle ein paar nötige »Komponenten« für deine App:

* **Bootstrap**: assets/**locations.js**:

  ```js
  import React from 'react';
  import ReactDOM from 'react-dom';
  import Locations from "./jsx/components/locations";
  
  ReactDOM.render(
      <Locations/>
      ,
      document.getElementById('root-locations')
  );
  ```

* **Location Root**: assets/jsx/components/**locations.js**:

  ```js
  import React, {useEffect, useState} from "react";
  import LocationList from "./locations/location-list";
  
  const Locations = () => {
      const [locations, setLocations] = useState(0);
  
      useEffect(() => {
          fetch(ROUTE_LOCATIONS)
              .then(response => response.json())
              .then(response => {
                  setLocations(response);
              });
      }, []);
  
      return (
          <>
              {locations ?
                  <LocationList locations={locations}/>
                  :
                  <p className={"text-center"}>Lade Daten…</p>
              }
          </>
      )
  };
  
  export default Locations;
  ```

* **Location List**: assets/jsx/components/locations/**location-list.js**:

  ```js
  import React from "react";
  import LocationItem from "./location-item";
  
  const LocationList = (props) => {
      return (
          <>
              <div className="grid grid-cols-3 gap-4">
                  {
                      props.locations.map((location, idx) => (
                          <LocationItem key={idx} location={location}/>
                      ))
                  }
              </div>
          </>
      )
  };
  
  export default LocationList;
  ```

* **Location Item**: assets/jsx/components/locations/**location-item.js**:

  ```js
  import React, {useEffect} from "react";
  
  const LocationItem = (props) => {
      return (
          <>
              <div className="col-span-1">
                  <div className="max-w-sm rounded overflow-hidden shadow-lg">
                      <div className="px-6 py-4">
                          <div className="font-bold text-xl mb-2">{props.location.title}</div>
                          <p className="text-gray-700 text-base">{props.location.teaser}</p>
                      </div>
                  </div>
              </div>
          </>
      );
  };
  
  export default LocationItem;
  
  ```

* Dann noch kompilieren:

  ```bash
  yarn encore dev
  ```

* Und testen!



#  Schlusswort

Wie du siehst ist das ganze zwar recht umfangreich, aber machbar. Wir hoffen, du bist mit allem klar gekommen und hattest wenige bis gar keine Probleme.

Solltest du Verbesserungsvorschläge, Anregungen oder Kritik haben, schick uns doch bitte eine Mail an t3ug@sota-studio.de.



# Anhang A: Cheat Sheet

## SSH Terminal Basics

| Befehl                                      | Beschreibung                                                 |
| ------------------------------------------- | ------------------------------------------------------------ |
| touch DATEINAME                             | Datei erstellen                                              |
| mkdir ORDNERNAME                            | Ordner erstellen                                             |
| ssh-keygen -t ed25519 -C "email@domain.de"  | Generiert einen lokalen 'private' und einen 'public' SSH-Key. |
| ssh-add -K ~/.ssh/[PRIVATKEY]               | Fügt den Private-Key zum SSH Agent hinzu                     |
| pbcopy < ~/.ssh/[PUBLICKEY].pub             | Fügt den Public-Key in die Zwischenablage ein                |
| tar -xvzf [NAMEDERDATEI].tar.gz             | Entpackt eine Datei                                          |
| tar cfvz [NAMEDERDATEI].tar.gz verzeichnis/ | Packt ein komplettes Verzeichnis                             |



## Git Basics

| Befehl                    | Beschreibung                                       |
| ------------------------- | -------------------------------------------------- |
| git add DATEINAME         | Einzelne Datei für commit vormerken         |
| git add -A                | Alle geänderten Dateien für commit vormerken   |
| git commit -m 'Kommentar' | Commit erstellen                |
| git push                  | Commit in Repository übertragen       |
| git pull                  | Lädt den aktuellen Stand aus dem Repository  |
| git status                | Zeigt die aktuellen Änderungen an                  |



## DDEV Basics

| Befehl        | Beschreibung|
| ------------- | ----------- |
| ddev describe | Übersicht Informationen zu Datenbank, Mailserver, etc. |
| ddev import-db --src=build/Snapshots/{dateiname}.sql | Datenbankdumb importieren |
| ddev export-db --gzip=false --file=build/Snapshots/$(date +%Y-%m-%d-%H%M).sql | Einen aktuellen Datenbankdumb erstellen |
| ddev help | Übersicht aller Befehle |



## Nützliche Helfer

Datenbank Dump als SQL exportieren:

```
ddev export-db --gzip=false --file=build/Snapshots/$(date +%Y-%m-%d-%H%M).sql
```



# Anhang B: Projekt klonen

Du willst dieses Projekt klonen und selber daran herum experimentieren?

```
git clone git@bitbucket.org:ideegestalten/t3r.typo3cms.10.git T3r.TYPO3CMS.10
cd T3r.TYPO3CMS.10
ddev start
ddev composer install
ddev import-db --src=build/Snapshots/[LATEST-FILE]
ddev restart
yarn install
yarn encore dev
```



# Anhang C: Troubleshooting

Probleme entdeckt? Sprich uns auf [Slack](https://typo3.slack.com/archives/C087T23DZ) an.



### Immer auf dem aktuellen Stand im Team bleiben

```bash
git pull # letzten Stand abholen
composer install # Packete nachinstallieren
yarn install # Packete nachinstallieren
yarn encore dev # Assets neu kompilieren
ddev import-db --src=build/Snapshots/[LATEST-FILE] # letzten Dump einspielen
```

