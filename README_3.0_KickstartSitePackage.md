# 3. Kickstart Site Package

**Vorgetragen von Ralf Hüskes – ca. 30 Minuten.**

Zu einer modernen Webentwicklung mit TYPO3 CMS gehört das Pattern eines »Site Package«. Dabei gilt die Faustregel: ein Site Package je Website, Microsite und Landingpage. Durch die Nutzung von Webpack Encore und einer sauberen Struktur, können sich Website problemlos Assets teilen.

Die Vorteile zusammengefasst:

* Konfigurationsdateien sind geschützt vor Fremdzugriffen von Redakteuren und außen
* Autoloading von PageTS
* Autoloading von TypoScript
* Statisches PageTS Template
* Statisches TypoScript Template
* Abhängigkeiten verwalten
* Saubere Versionierung
* Hohe Wiederverwendbarkeit
  

## 3.1. Erstellung

Site Package unter https://www.sitepackagebuilder.com/ erstellen und herunterladen.

- TYPO3 Version: 10.4
- Base Package: Fluid Styled Content
- Title: Site
- Author
  - Name: [YOUR NAME]
  - Email: [YOUR E-MAIL]
  - Company: TYPO3 Stammtisch Ruhr
  - Homepage: https://typo3-ruhr.org/

Der Extension Key lautet dann »site«, der composer Package Name »typo3-stammtisch-ruhr/site«.



## 3.2. Installation

- Ordner »packages« auf Root-Ebene erstellen

```bash
mkdir packages
```

- Site Package Ordner »site« in den neuen Ordner »packages« legen (Der Ordner Packages im Root-Verzeichnis unseres Projekte ist unser lokales Repository in dem wir unsere eigenen Erweiterungen ablegen)

- Wichtig: composer.json der Extension um eine Versionsnummer ergänzen – **"version": "1.0.0",**

```js
"version": "1.0.0",
```

- Wichtig: Site Package als »Path«-Quelle in globaler composer.json des Projektes hinzufügen

```javascript
"repositories": [
	{ "type": "composer", "url": "https://composer.typo3.org/" },
	{
		"type": "path",
		"url": "packages/site"
	}
],
```

- Site Package via composer installieren

```bash
ddev composer req typo3-stammtisch-ruhr/site 
```

- Wenn das Site Package erfolgreich installiert wurde, muss man sich im TYPO3 Backend nur noch eine Startseite anlegen, diese als Dokument Root (Behavior - Use as Root Page) definieren und sichtbar setzen. 
- Im nächsten Schritt nicht vergessen für die Root-Seite ein neues Template zu erstellen und das Site Package einzubinden (TypoScript)

Ab diesem Punkt sollte die Startseite bereits eine (unschöne) Ausgabe im Frontend zeigen.


## 3.3. Konfiguration: Backendlayout anlegen

- **Landingpage.html als Template anlegen und Pfad zum Layout anpassen**
  t3r.typo3cms.10/packages/site/Resources**/Private/Templates/Page/**

- **Landingpage.html als Layout anlegen **
  t3r.typo3cms.10/packages/site/Resources**/Private/Layouts/Page/**

- **landingpage.tsconfig erzeugen und Backendlayout anpassen ('default' suchen/ersetzen)**
  t3r.typo3cms.10/packages/site/Configuration/**TsConfig/Page/Mod/WebLayout/BackendLayouts/**

  - row1 2x kopieren (stage / normal / footer)

- **Label in locallang_be.xlf ergänzen**

```xml
<trans-unit id="backend_layout.landingpage">
	<source>Landingpage</source>
</trans-unit>
<trans-unit id="backend_layout.column.stage">
	<source>Stage</source>
</trans-unit>
<trans-unit id="backend_layout.column.footer">
	<source>Footer</source>
</trans-unit>
```

-  **Template in setup.typoscript einfügen**
  t3r.typo3cms.10/packages/site/Configuration/TypoScript/setup.typoscript

```typescript
templateName = TEXT
templateName {
      cObject = TEXT
      cObject {
          data = pagelayout
          required = 1
          case = uppercamelcase
          split {
              token = pagets__
              cObjNum = 1
              1.current = 1
          }
          #########################
					# New BE Layouts einfügen
					#########################
          landingpage = TEXT
          landingpage.value = Landingpage
      }
      ifEmpty = Default
  }
```

- **Neue Backendspalten im Template übernehmen**
  t3r.typo3cms.10/packages/site/Resources/Private/**Templates/Page/Landingpage.html**

```html
  <f:section name="Stage">
    <f:cObject typoscriptObjectPath="lib.dynamicContent" data="{colPos: '0'}" />
  </f:section>
  
  <f:section name="normal">
      <f:cObject typoscriptObjectPath="lib.dynamicContent" data="{colPos: '1'}" />
  </f:section>
  
  <f:section name="footer">
      <f:cObject typoscriptObjectPath="lib.dynamicContent" data="{colPos: '2'}" />
  </f:section>
```

- **Grafik für das Backendlayout kopieren und modifizieren.**

  Damit Ihr die Spalten sofort bei der Auswahl anhand der Grafik erkennt, empfiehlt es sich ein eigenes Icon anzulegen.
  t3r.typo3cms.10/packages/site/Resources/**Public/Images/BackendLayouts/Landingpage.png**



