<?php

/**
 * Extension Manager/Repository config file for ext "site".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'Site',
    'description' => '',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '10.2.0-10.4.99',
            'fluid_styled_content' => '10.2.0-10.4.99',
            'rte_ckeditor' => '10.2.0-10.4.99',
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Typo3StammtischRuhr\\Site\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Ralf Hüskes',
    'author_email' => 'info@ideegestalten.net',
    'author_company' => 'TYPO3 Stammtisch Ruhr',
    'version' => '1.0.0',
];
