<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Typo3StammtischRuhr.Locations',
            'Ll',
            [
                'Location' => 'list, show'
            ],
            // non-cacheable actions
            [
                'Location' => ''
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Typo3StammtischRuhr.Locations',
            'Ld',
            [
                'Location' => 'list, show'
            ],
            // non-cacheable actions
            [
                'Location' => ''
            ]
        );
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Typo3StammtischRuhr.Locations',
            'LlApi',
            [
                'LocationApi' => 'list, show',
            ],
            []
        );


        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins {
                    elements {
                        ll {
                            iconIdentifier = locations-plugin-ll
                            title = LLL:EXT:locations/Resources/Private/Language/locallang_db.xlf:tx_locations_ll.name
                            description = LLL:EXT:locations/Resources/Private/Language/locallang_db.xlf:tx_locations_ll.description
                            tt_content_defValues {
                                CType = list
                                list_type = locations_ll
                            }
                        }
                        ld {
                            iconIdentifier = locations-plugin-ld
                            title = LLL:EXT:locations/Resources/Private/Language/locallang_db.xlf:tx_locations_ld.name
                            description = LLL:EXT:locations/Resources/Private/Language/locallang_db.xlf:tx_locations_ld.description
                            tt_content_defValues {
                                CType = list
                                list_type = locations_ld
                            }
                        }
                    }
                    show = *
                }
           }'
        );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

			$iconRegistry->registerIcon(
				'locations-plugin-ll',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:locations/Resources/Public/Icons/user_plugin_ll.svg']
			);

			$iconRegistry->registerIcon(
				'locations-plugin-ld',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:locations/Resources/Public/Icons/user_plugin_ld.svg']
			);

    }
);
