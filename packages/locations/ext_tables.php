<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Locations',
            'Ll',
            'Location List'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Locations',
            'Ld',
            'Location Detail'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('locations', 'Configuration/TypoScript', 'Locations');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_locations_domain_model_location', 'EXT:locations/Resources/Private/Language/locallang_csh_tx_locations_domain_model_location.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_locations_domain_model_location');

    }
);
