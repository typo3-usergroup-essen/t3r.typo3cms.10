#
# Table structure for table 'tx_locations_domain_model_location'
#
CREATE TABLE tx_locations_domain_model_location (

	title varchar(255) DEFAULT '' NOT NULL,
	teaser text,
	description text,
	street varchar(255) DEFAULT '' NOT NULL,
	street_no varchar(255) DEFAULT '' NOT NULL,
	zip varchar(255) DEFAULT '' NOT NULL,
	city varchar(255) DEFAULT '' NOT NULL,
	country varchar(255) DEFAULT '' NOT NULL,
	phone varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	contact_person varchar(255) DEFAULT '' NOT NULL

);
