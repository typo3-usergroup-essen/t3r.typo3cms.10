<?php

namespace Typo3StammtischRuhr\Locations\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\View\JsonView;
use Typo3StammtischRuhr\Locations\Domain\Repository\LocationRepository;

class LocationApiController extends ActionController
{
    /** @var JsonView */
    protected $view;

    /** @var string */
    protected $defaultViewObjectName = JsonView::class;

    /** @var LocationRepository */
    private $locationRepository;

    public function __construct(LocationRepository $locationRepository)
    {
        $this->locationRepository = $locationRepository;
    }

    public function listAction()
    {
        $locations = $this->locationRepository->findAll();
        $this->view->setConfiguration([
            'locations' => [
                '_descendAll' => [
                    '_only' => ['uid', 'title', 'teaser'],
                ],
            ],
        ]);
        $this->view->setVariablesToRender(['locations']);
        $this->view->assign('locations', $locations);
    }
}
