<?php
namespace Typo3StammtischRuhr\Locations\Controller;


/***
 *
 * This file is part of the "Locations" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 
 *
 ***/
/**
 * LocationController
 */
class LocationController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * locationRepository
     * 
     * @var \Typo3StammtischRuhr\Locations\Domain\Repository\LocationRepository
     */
    protected $locationRepository = null;

    /**
     * @param \Typo3StammtischRuhr\Locations\Domain\Repository\LocationRepository $locationRepository
     */
    public function injectLocationRepository(\Typo3StammtischRuhr\Locations\Domain\Repository\LocationRepository $locationRepository)
    {
        $this->locationRepository = $locationRepository;
    }

    /**
     * action list
     * 
     * @return void
     */
    public function listAction()
    {
        $locations = $this->locationRepository->findAll();
        $this->view->assign('locations', $locations);
    }

    /**
     * action show
     * 
     * @param \Typo3StammtischRuhr\Locations\Domain\Model\Location $location
     * @return void
     */
    public function showAction(\Typo3StammtischRuhr\Locations\Domain\Model\Location $location)
    {
        $this->view->assign('location', $location);
    }
}
