# 6. Tailwind im Site Package verwenden

Das Markup haben wir in Layout, Templates und Partials aufgeteilt, wobei wir immer versuchen die Dateien so übersichtlich wie möglich zu halten und möglichst viel in Partials auszugliedern. 


**6.1. Eigene Styles hinzufügen**

Für unser Beispiel reichen die Bordmittel von Tailwind nicht aus und ich möchte einen eigenen Style ergänzen
t3r.typo3cms.10/assets**/scss/app.scss**

```
.gradient {
    background: linear-gradient(90deg, #d53369 0%, #daae51 100%);
}
```


**6.2. Eigene Styles hinzufügen**
Neben dem eigenen Style benötigen wir zudem eigenes JavaScript das ebenfalls im Ordner assets/ abgelegt wirdt3r.typo3cms.10**/assets/js/shame.js**


**6.3 app.scss und shame.js in app.js ergänzen**

Damit ich meine eigenen Styles und JS in webpack encore verwenden kann muss ich das ganze noch in meiner app.js einbinden
t3r.typo3cms.10**/assets/app.js**

```javascript
require('./scss/tailwind.scss');
require('./scss/app.scss');
require('./js/shame.js');
```


**6.4 CSS und JS neu compilieren**
Damit die Ergänzungen übernommen werden muss ich webpack encore sagen, dass die modifizierten Styles und Scripte neu erzeugt werden müssen

```bash
yarn encore dev
```


**6.5 Class in Bodytag einfügen**

Damit ich die Ausgabe wie in meiner Templatevorgabe erhalte fehlen mir noch ein paar Klassen im Body-Tag der Seite. Hierfür erweitere ich das page Element in meinem typoscript
t3r.typo3cms.10/packages/site/**Configuration/TypoScript/setup.typoscript**

```typoscript
page.bodyTagAdd = class="leading-normal tracking-normal text-white gradient"
```

