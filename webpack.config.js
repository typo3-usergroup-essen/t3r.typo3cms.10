var Encore = require('@symfony/webpack-encore');

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('packages/site/Resources/Public/build/')
    .setPublicPath('/typo3conf/ext/site/Resources/Public/build/')
    .setManifestKeyPrefix('build/')

    .addEntry('app', './assets/app.js')
    .addEntry('locations', './assets/locations.js')

    .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })
        .enableSassLoader(function (options) {}, {
            resolveUrlLoader: false
        })
        .enablePostCssLoader(function(options) {
            options.config = {
                path: 'config/postcss.config.js'
            }
        })


    .enableReactPreset()
;

module.exports = Encore.getWebpackConfig();
