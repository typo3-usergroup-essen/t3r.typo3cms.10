# 7.0 Individuelle Inhaltselemente

  In unserer Session zeigen wir dir:

  * wie du ein bestehendes Core-Element um Funktionen ergänzt
  * wie du ein eigenes, neues Inhaltselement erstellst

## 7.1. TYPO3 CE-Templates anpassen


Das schöne am Site Package als Fluid Styled Content Konfiguration ist, dass ich mich nicht mehr um die Konfiguration kümmern muss, wenn ich ein vorhandenes Core Element anpassen möchte. Dafür muss man nur wissen wo sich die Elemente befinden und wo ich sie hinverschieben muss. 

  -  **Template 'Textpic.html' kopieren:**
    t3r.typo3cms.10/public/typo3/**sysext/fluid_styled_content/Resources/Private/Templates**
    nach 
    t3r.typo3cms.10**/packages/site/Resources/Private/Templates/ContentElements**

  -  **Layout 'Default' kopieren:**
    t3r.typo3cms.10/public/typo3/**sysext/fluid_styled_content/Resources/Private/Layouts**
    nach 
    t3r.typo3cms.10**/packages/site/Resources/Private/Layouts/ContentElements**

  -  **Layoutelement in CE erweitern bzw. unnötige Felder ausblenden:**
    t3r.typo3cms.10/**packages/site/Configuration/TsConfig/Page/TCEFORM.tsconfig**

```
TCEFORM {
    pages {

    }
    tt_content {
        date.disabled = 1
        header_layout.disabled = 1
        image_zoom.disabled = 1
        sectionIndex.disabled = 1
        linkToTop.disabled = 1
        layout {
            types {
                textpic {
                    removeItems = 1,2,3
                    addItems {
                        4 = Call to Action
                    }
                }
            }
        }
    }
}
```

**Switch in Layouts/Default.html **
t3r.typo3cms.10/packages/site/Resources/**Private/Layouts/ContentElements/Default.html**

```
<f:switch expression="{data.layout}">
    <f:case value="4">
        <f:render section="CallToAction" />
    </f:case>
    <f:defaultCase>
       <!-- Hier folgt der reguläre Aufruf der Default.html -->
    </f:defaultCase>
</f:switch>
```

**Individuelle Ausgabe in CE Textpics**
t3r.typo3cms.10/packages/site/Resources/Private/Templates/ContentElements/Textpic.html

*Über Conditions können Tailwind-Klassen sehr bequem und einfach an ein Element geheftet werden, um so die Ausgabe zusätzlich zu beinflussen. Z.b.: Text zentrieren wenn kein Bild eingebaut wurde* 

```
<f:section name="CallToAction">
    <!--Left Col-->
    <div class="flex flex-col {f:if(condition='{files}',then:'w-full md:w-2/5 items-start text-center md:text-left',else:'mt-8 w-full items-center')} justify-center ">
        <p class="uppercase tracking-loose">{data.subheader}</p>
        <h1 class="my-4 text-5xl font-bold leading-tight {f:if(condition='{files}',then:'text-5xl', else:'text-4xl uppercase')}">{data.header}</h1>
        <div class="leading-normal text-2xl mb-8">
            <f:format.html>{data.bodytext}</f:format.html>
        </div>
        <f:if condition="{data.header_link}">
            <f:link.typolink parameter="{data.header_link}" class="mx-auto lg:mx-0 hover:underline bg-white text-gray-800 font-bold rounded-full my-6 py-4 px-8 shadow-lg">Read more</f:link.typolink>
        </f:if>
    </div>
    <f:if condition="{files}">
        <!--Right Col-->
        <div class="w-full md:w-3/5 py-6 text-center">
            <f:for each="{files}" as="filesItem" iteration="iteration">
                <figure class="mb-6">
                    <f:image image="{filesItem}" class="w-full md:w-4/5 z-50" />
                </figure>
            </f:for>
        </div>
    </f:if>
</f:section>
```




## 7.2 Eigenes Contentelement für ein Accordion

**7.2.1 Datenbankstruktur für mein Element anlegen**
t3r.typo3cms.10/packages/site/ext_tables.sql

```
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
    tx_site_accordion_item int(11) unsigned DEFAULT '0',
    );

#
# Table structure for table 'tx_site_accordion_item'
#
CREATE TABLE tx_site_accordion_item (

    tt_content int(11) unsigned DEFAULT '0',
    header varchar(255) DEFAULT '' NOT NULL,
    teaser text,
    media int(11) unsigned DEFAULT '0',
    bodytext text,

);
```


**7.2.2 Neues Inlineenelement im Backend für die Ausgabe genehmigen**

(Wenn Ihr das nicht für jedes neue InlineElement macht, wird euch das CE vielleicht angezeigt - beim Speichern bekommt ihr aber einen Fehler ausgeworfen)
    t3r.typo3cms.10/**packages/site/ext_tables.php**
    

```
/***************
 * Allow Custom Records on Standard Pages
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_site_accordion_item');
```


**7.2.3 Typoscript Konfigurationen und Icon-Registrierung einfügen**
t3r.typo3cms.10/packages/site/ext_localconf.php

Einmalige Ergänzungen :

```
/***************
 * Add content rendering configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['FE']['contentRenderingTemplates']['site'] = 'EXT:site/Configuration/TypoScript/ContentElement/';
```

```
/***************
 * PageTS
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site/Configuration/TsConfig/ContentElement/All.tsconfig">');
```


Muss für individuelle Icons ergänzt werden:

```
/***************
 * Register Icons
 */
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$iconRegistry->registerIcon(
    'systeminformation-site',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:site/Resources/Public/Icons/SystemInformation/site.svg']
);
/* Add more icons for other content elementes */
$icons = [
    'accordion',
    'accordion-item'
];
foreach ($icons as $icon) {
    $iconRegistry->registerIcon(
        'content-site-' . $icon,
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:site/Resources/Public/Icons/ContentElements/' . $icon . '.svg']
    );
}
```


**7.2.4  Neue Ordner und neue Grafiken als SVG anlegen**
t3r.typo3cms.10/packages**/site/Resources/Public/Icons/**

- ContentElements/accordion.svg
- ContentElements/accordion-item.svg
- SystemInformation/site.svg

-  **Typoscript Setup und Konstanten anlegen und ins Site-Package als Abhängigkeiten einfügen**


Konstanten:
t3r.typo3cms.10/packages/site/Configuration**/TypoScript/constants.typoscript**

```
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site/Configuration/TypoScript/ContentElement/constants.typoscript">
```


Setup:
t3r.typo3cms.10/packages/site/Configuration**/TypoScript/setup.typoscript**

```
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site/Configuration/TypoScript/ContentElement/setup.typoscript">
```


Setup und Konstanten für für Custom ContentElements anlegen:
t3r.typo3cms.10/packages/site/Configuration/**TypoScript/ContentElement/**

.../ContentElement/**setup.typoscript**

```
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site/Configuration/TypoScript/ContentElement/Element/Accordion.typoscript">
```


.../ContentElement**/constants.typoscript**

```
plugin.site_contentelements {
    view {
        layoutRootPath = EXT:site/Resources/Private/Layouts/ContentElements/
        partialRootPath = EXT:site/Resources/Private/Partials/ContentElements/
        templateRootPath = EXT:site/Resources/Private/Templates/ContentElements/
    }
}
```


Natürlich brauche ich dann noch die Config des jeweiligen Elementes 
    ...TsConfig/ContentElement**/Element/Accordion.typoscript**

```
############################################
#### CTYPE:accordion ####
############################################
tt_content.accordion >
tt_content.accordion =< lib.contentElement
tt_content.accordion {
    ################
    ### TEMPLATE ###
    ################
    templateName = Accordion

    ##########################
    ### DATA PREPROCESSING ###
    ##########################
    dataProcessing {
        20 = TYPO3\CMS\Frontend\DataProcessing\DatabaseQueryProcessor
        20 {
            table = tx_site_accordion_item
            pidInList.field = pid
            where {
                data = field:uid
                intval = 1
                wrap = tt_content=|
            }
            orderBy = sorting
            dataProcessing {
                10 = TYPO3\CMS\Frontend\DataProcessing\FilesProcessor
                10 {
                    references.fieldName = media
                    as = files
                }
            }
        }
    }
}
```


Mein neues Element möchte ich natürlich auch im Backend anzeigen lassen, dafür lege ich im Order TsConfig für jedes Element eine neue ts.config an. 

t3r.typo3cms.10/packages/site/Configuration/**TsConfig/ContentElement/All.tsconfig**

```
##########################
#### CONTENT ELEMENTS ####
##########################
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:site/Configuration/TsConfig/ContentElement/Element" extensions="tsconfig">
```

All.tsconfig referenziert dabei in den Ordner Element und berücksichtigt jede ts.config die in dem Ordner Element angelegt wurde.

t3r.typo3cms.10/packages/site/Configuration/**TsConfig/ContentElement/Element/Accordion.tsconfig**

 In der [ELEMENTBEZEICHNUNG].tsconfig bestimme ich u.a. unter welchem TAB das Element angezeigt werden soll. 
 Z.B.: "Typical page content":  .wizardItems.**common**

```
####################################
#### NEW CONTENT ELEMENT WIZARD ####
####################################
mod.wizards.newContentElement.wizardItems.common {
    elements {
        accordion {
            iconIdentifier = content-site-accordion
            title = LLL:EXT:site/Resources/Private/Language/Backend.xlf:content_element.accordion
            description = LLL:EXT:site/Resources/Private/Language/Backend.xlf:content_element.accordion.description
            tt_content_defValues {
                CType = accordion
            }
        }
    }
    show := addToList(accordion)
}
```

**Sprachdatei nicht vergessen**
t3r.typo3cms.10/packages/site/Resources/Private/Language/Backend.xlf

```
<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<xliff version="1.0">
    <file source-language="en" datatype="plaintext" original="messages" date="2020-05-20UTC17:00:290">
        <header>
            <authorName></authorName>
            <authorEmail></authorEmail>
        </header>
        <body>
            <trans-unit id="content_element.accordion">
                <source>Awesome Accordion</source>
            </trans-unit>
            <trans-unit id="content_element.accordion.description">
                <source>An inline relation record element for fancy accordion magic</source>
            </trans-unit>
            <trans-unit id="accordion_item">
                <source>Accordion items</source>
            </trans-unit>
            <trans-unit id="accordion_item.header">
                <source>Title</source>
            </trans-unit>
            <trans-unit id="accordion_item.teaser">
                <source>Teaser</source>
            </trans-unit>
            <trans-unit id="accordion_item.bodytext">
                <source>Bodytext</source>
            </trans-unit>
            <trans-unit id="accordion_item.media">
                <source>Images</source>
            </trans-unit>
        </body>
    </file>
</xliff>
```


Damit TYPO3 erkennt welche Felder in meinem Element benötigt werden, muss ich ein allgemeines TCA  (Table Configuration Array) und eins für mein Inline-Element erzeugen. Den Aufbau kann man sich gut bei anderen Extensions abschauen, bzw in der offiziellen TYPO3 Dokumentation: https://docs.typo3.org/m/typo3/reference-tca/master/en-us/#

t3r.typo3cms.10/packages/site/**Configuration/TCA/Overrides/content_element_accordion.php**

```
<?php

/* *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') || die();

/**
 * Temporary variables
 */
$extensionKey = 'site';


/***************
 * Add Content Element
 */
if (!is_array($GLOBALS['TCA']['tt_content']['types']['accordion'])) {
    $GLOBALS['TCA']['tt_content']['types']['accordion'] = [];
}

/***************
 * Add content element PageTSConfig
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    $extensionKey,
    'Configuration/TsConfig/ContentElement/Element/accordion.tsconfig',
    'Site Package Content Element: Accordion Element'
);

/***************
 * Add content element to selector accordion
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'LLL:EXT:site/Resources/Private/Language/Backend.xlf:content_element.accordion',
        'accordion',
        'content-site-accordion'
    ],
    '--div--',
    'after'
);

/***************
 * Assign Icon
 */
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['accordion'] = 'content-site-accordion';

/***************
 * Configure element type
 */
$GLOBALS['TCA']['tt_content']['types']['accordion'] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types']['accordion'],
    [
        'showitem' => '
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
                tx_site_accordion_item,
            --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                --palette--;;language,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                --palette--;;hidden,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
                rowDescription,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
        '
    ]
);

/***************
 * Register fields
 */
$GLOBALS['TCA']['tt_content']['columns'] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['columns'],
    [
        'tx_site_accordion_item' => [
            'label' => 'LLL:EXT:site/Resources/Private/Language/Backend.xlf:accordion_item',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_site_accordion_item',
                'foreign_field' => 'tt_content',
                'appearance' => [
                    'useSortable' => true,
                    'showSynchronizationLink' => true,
                    'showAllLocalizationLink' => true,
                    'showPossibleLocalizationRecords' => true,
                    'showRemovedLocalizationRecords' => false,
                    'expandSingle' => true,
                    'enabledControls' => [
                        'localize' => true,
                    ]
                ],
                'behaviour' => [
                    'mode' => 'select',
                ]
            ]
        ],
    ]
);
```

**
**TCA für das Accordion-Item anlegen**
t3r.typo3cms.10/packages/site/**Configuration/TCA/tx_site_accordion_item.php**

```
<?php

/*
 * This file is part of the package bk2k/bootstrap-package.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('lang')) {
    $generalLanguageFile = 'EXT:lang/Resources/Private/Language/locallang_general.xlf';
} else {
    $generalLanguageFile = 'EXT:core/Resources/Private/Language/locallang_general.xlf';
}

return [
    'ctrl' => [
        'label' => 'header',
        'sortby' => 'sorting',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'title' => 'LLL:EXT:site/Resources/Private/Language/Backend.xlf:accordion_item',
        'delete' => 'deleted',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'hideTable' => true,
        'hideAtCopy' => true,
        'prependAtCopy' => 'LLL:' . $generalLanguageFile . ':LGL.prependAtCopy',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'languageField' => 'sys_language_uid',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'typeicon_classes' => [
            'default' => 'content-site-accordion-item',
        ]
    ],
    'interface' => [
        'showRecordFieldList' => '
            hidden,
            tt_content,
            header
        ',
    ],
    'types' => [
        '1' => [
            'showitem' => '
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                header,
                teaser,
                bodytext,
                media,              
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.visibility;visibility,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
                --palette--;;hiddenLanguagePalette,
            '
        ],
    ],
    'palettes' => [
        '1' => [
            'showitem' => ''
        ],
        'access' => [
            'showitem' => '
                starttime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:starttime_formlabel,
                endtime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:endtime_formlabel
            '
        ],
        'size' => [
            'showitem' => '
                width,
                height
            '
        ],
        'general' => [
            'showitem' => '
                tt_content
            '
        ],
        'visibility' => [
            'showitem' => '
                hidden;LLL:EXT:site/Resources/Private/Language/Backend.xlf:accordion_item
            '
        ],
        // hidden but needs to be included all the time, so sys_language_uid is set correctly
        'hiddenLanguagePalette' => [
            'showitem' => 'sys_language_uid, l10n_parent',
            'isHiddenPalette' => true,
        ],
    ],
    'columns' => [
        'tt_content' => [
            'exclude' => true,
            'label' => 'LLL:EXT:site/Resources/Private/Language/Backend.xlf:accordion_item.tt_content',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tt_content',
                'foreign_table_where' => 'AND tt_content.pid=###CURRENT_PID### AND tt_content.CType="list"',
                'maxitems' => 1,
                'default' => 0,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:' . $generalLanguageFile . ':LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:hidden.I.0'
                    ]
                ]
            ]
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:' . $generalLanguageFile . ':LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime',
                'default' => 0
            ],
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly'
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:' . $generalLanguageFile . ':LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ]
            ],
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly'
        ],
        'sys_language_uid' => [
            'exclude' => 1,
            'label' => 'LLL:' . $generalLanguageFile . ':LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => [
                    [
                        'LLL:' . $generalLanguageFile . ':LGL.allLanguages',
                        -1
                    ],
                    [
                        'LLL:' . $generalLanguageFile . ':LGL.default_value',
                        0
                    ]
                ],
                'allowNonIdValues' => true,
            ]
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:' . $generalLanguageFile . ':LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        '',
                        0
                    ]
                ],
                'foreign_table' => 'tx_site_accordion_item',
                'foreign_table_where' => 'AND tx_site_accordion_item.pid=###CURRENT_PID### AND tx_site_accordion_item.sys_language_uid IN (-1,0)',
                'default' => 0
            ]
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'header' => [
            'exclude' => true,
            'label' => 'LLL:EXT:site/Resources/Private/Language/Backend.xlf:accordion_item.header',
            'config' => [
                'type' => 'input',
                'size' => 50,
                'eval' => 'trim'
            ],
        ],
        'teaser' => [
            'label' => 'LLL:EXT:site/Resources/Private/Language/Backend.xlf:accordion_item.teaser',
            'l10n_mode' => 'prefixLangTitle',
            'l10n_cat' => 'text',
            'config' => [
                'type' => 'text',
                'cols' => '40',
                'rows' => '5',
                'softref' => 'typolink_tag,images,email[subst],url',
                'enableRichtext' => false,
                'richtextConfiguration' => 'default'
            ],
        ],
        'bodytext' => [
            'label' => 'LLL:EXT:site/Resources/Private/Language/Backend.xlf:accordion_item.bodytext',
            'l10n_mode' => 'prefixLangTitle',
            'l10n_cat' => 'text',
            'config' => [
                'type' => 'text',
                'cols' => '80',
                'rows' => '15',
                'softref' => 'typolink_tag,images,email[subst],url',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default'
            ],
        ],
        'media' => [
            'exclude' => true,
            'label' => 'LLL:EXT:site/Resources/Private/Language/Backend.xlf:accordion_item.media',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'media',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'overrideChildTca' => [
                        'types' => [
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_UNKNOWN => [
                                'showitem' => '
                                    --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                                'showitem' => '
                                    --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '
                                    --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                                'showitem' => '
                                    --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.audioOverlayPalette;audioOverlayPalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                                'showitem' => '
                                    --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.videoOverlayPalette;videoOverlayPalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                                'showitem' => '
                                    --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ]
                        ]
                    ]
                ],
                $GLOBALS['TYPO3_CONF_VARS']['SYS']['mediafile_ext']
            ),
        ]
    ]
];
```


**Fluid Template anlegen**
Damit der ganze Spaß auch im Frontend richtig ausgegeben wird, muss ich natürlich noch mein Fluid Template anlegen

t3r.typo3cms.10/packages/site/Resources/**Private/Templates/ContentElements/Accordion.html**

```
<html xmlns:f="http://typo3.org/ns/TYPO3/CMS/Fluid/ViewHelpers" data-namespace-typo3-fluid="true">
  <div class="w-full mx-auto py-12">
      <f:if condition="{data.header}">
          <p class="mb-4 font-semibold px-1 text-xl md:text-2xl">{data.header}</p>
      </f:if>
      <f:if condition="{records}">
          <div class="shadow-md">
              <f:for each="{records}" as="record" iteration="iteration">
              <div class="tab w-full overflow-hidden border-t">
                  <input class="absolute opacity-0" id="tab-single-{record.data.uid}" type="radio" name="tabs2">
                  <label class="block p-5 leading-normal cursor-pointer" for="tab-single-{record.data.uid}">{record.data.header}</label>
                  <div class="tab-content overflow-hidden border-l-2 bg-gray-100 border-pink-500 leading-normal">
                      <f:if condition="{record.files}">
                          <f:for each="{record.files}" as="filesItem" iteration="iteration">
                              <figure class="mb-8 mt-4">
                                  <f:image class="rounded-full mx-6 border-2 border-pink-500 shadow-lg" image="{filesItem}" width="100" height="100c"/>
                                  <f:if condition="{filesItem.description}">
                                      <figcaption>
                                          {filesItem.description}
                                      </figcaption>
                                  </f:if>
                              </figure>
                          </f:for>
                      </f:if>
                      <f:if condition="{record.data.teaser}">
                          <div class="text-blue-1000 m-6 font-semibold">
                              <f:format.html>{record.data.teaser}</f:format.html>
                          </div>
                      </f:if>
                      <f:if condition="{record.data.bodytext}">
                          <div class="m-6">
                              <f:format.html>{record.data.bodytext}</f:format.html>
                          </div>
                      </f:if>
                  </div>
              </div>
              </f:for>
          </div>
      </f:if>
  </div>
</html>
```

  

-  Jetzt noch im TYPO3 **Admin-Tool / Maintenance -> Analyze Database Structure** ausführen und die neue DB Struktur anlegen lassen

-  Cache leeren und prüfen ob mein Element angelegt, einsatzbereit und fehlerfrei funktioniert. 
