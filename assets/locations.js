import React from 'react';
import ReactDOM from 'react-dom';
import Locations from "./jsx/components/locations";

ReactDOM.render(
    <Locations/>
    ,
    document.getElementById('root-locations')
);
