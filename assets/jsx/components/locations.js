import React, {useEffect, useState} from "react";
import LocationList from "./locations/location-list";

const Locations = () => {
    const [locations, setLocations] = useState(0);

    useEffect(() => {
        fetch(ROUTE_LOCATIONS)
            .then(response => response.json())
            .then(response => {
                setLocations(response);
            });
    }, []);

    return (
        <>
            {locations ?
                <LocationList locations={locations}/>
                :
                <p className={"text-center"}>Lade Daten…</p>
            }
        </>
    )
};

export default Locations;
