import React from "react";
import LocationItem from "./location-item";

const LocationList = (props) => {
    return (
        <>
            <div className="grid grid-cols-3 gap-4">
                {
                    props.locations.map((location, idx) => (
                        <LocationItem key={idx} location={location}/>
                    ))
                }
            </div>
        </>
    )
};

export default LocationList;
