import React, {useEffect} from "react";

const LocationItem = (props) => {
    return (
        <>
            <div className="col-span-1">
                <div className="max-w-sm rounded overflow-hidden shadow-lg">
                    <div className="px-6 py-4">
                        <div className="font-bold text-xl mb-2">{props.location.title}</div>
                        <p className="text-gray-700 text-base">{props.location.teaser}</p>
                    </div>
                </div>
            </div>
        </>
    );
};

export default LocationItem;
